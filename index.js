let number_1 = Number(prompt("Number 1 = "));
let number_2 = Number(prompt("Number 2 = "));
let operation = prompt("Operation: ");

function Operation(n1, n2, o) {
    if (isNaN(n1)) return 'Error';
    if (isNaN(n2)) return 'Error';
    switch (o) {
        case '+':
            return n1 + n2;
        case '-':
            return n1 - n2;
        case '*':
            return n1 * n2;
        case '/':
            if(n2 == 0) return 'dev by zero';
            return n1 / n2;
        default:
            return 'Error';
    }
}

console.log(Operation(number_1, number_2, operation));